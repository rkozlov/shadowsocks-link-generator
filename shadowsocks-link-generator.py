import base64

def ss_gen():
    server = input('Server: ')
    port = input('Server port: ')
    method = input('Method: ')
    password = input('Password: ')
    hash = base64.b64encode(f'{method}:{password}'.encode())
    return f'ss://{hash.decode()}@{server}:{port}#{server}'

if __name__ == '__main__':
    print(ss_gen())
